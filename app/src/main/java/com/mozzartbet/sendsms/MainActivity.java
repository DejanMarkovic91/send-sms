package com.mozzartbet.sendsms;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.buchandersenn.android_permission_manager.PermissionManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import static android.R.attr.phoneNumber;
import static com.github.buchandersenn.android_permission_manager.callbacks.PermissionCallbacks.showPermissionDeniedSnackbar;
import static com.github.buchandersenn.android_permission_manager.callbacks.PermissionCallbacks.showPermissionShowRationaleSnackbar;
import static com.github.buchandersenn.android_permission_manager.callbacks.PermissionCallbacks.startPermissionGrantedActivity;

public class MainActivity extends AppCompatActivity {
    private SmsManager manager = SmsManager.getDefault();
    private final PermissionManager permissionManager = PermissionManager.create(this);
    private EditText message;
    private SpinnerDialog spinner;
    private View mainLayout;
    private int sent, failed, length, status_received, line_failed;
    private File messagesFolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        message = (EditText) findViewById(R.id.message);
        mainLayout = findViewById(R.id.activity_main);


        permissionManager.with(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .onPermissionDenied(showPermissionDeniedSnackbar(mainLayout, "SD card access denied!", "SETTINGS"))
                .onPermissionShowRationale(showPermissionShowRationaleSnackbar(mainLayout, "SD card access required to read file from downloads folder", "OK"))
                .request();

        permissionManager.with(Manifest.permission.SEND_SMS)
                .onPermissionDenied(showPermissionDeniedSnackbar(mainLayout, "Permission to send SMS messages denied!", "SETTINGS"))
                .onPermissionShowRationale(showPermissionShowRationaleSnackbar(mainLayout, "Permission to send SMS is required!", "OK"))
                .request();

        findViewById(R.id.send_sms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                messagesFolder = new File(Environment.getExternalStorageDirectory()
                        + "/Download/Messages");
                if (!messagesFolder.exists()) {//create if it does not exist
                    if (messagesFolder.mkdir()) {
                        showSnackbarMesssage(String.format("Folder %s created, please move files into it!", messagesFolder.getAbsolutePath()), false);
                    } else {
                        showSnackbarMesssage(String.format("Folder %s can't be created, maybe missing permission?", messagesFolder.getAbsolutePath()), true);
                    }
                } else if (messagesFolder.listFiles().length == 0) {
                    showSnackbarMesssage("Please put files in Messages directory!", true);
                } else {
                    String messageText = message.getText().toString();
                    if (messageText.isEmpty()) {
                        Snackbar.make(mainLayout, "Message can not be empty!", Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                    spinner = new SpinnerDialog(MainActivity.this);
                    spinner.show();
                    sent = 0;
                    failed = 0;
                    status_received = 0;
                    line_failed = 0;
                    length = 0;
                    File[] files = messagesFolder.listFiles();
                    for (File messagesFile : files) {
                        if (messagesFile.exists()) {
                            try {
                                BufferedReader buffreader = new BufferedReader(new InputStreamReader(new FileInputStream(messagesFile)));
                                String line = null;
                                while ((line = buffreader.readLine()) != null) {
                                    length++;
                                    Log.d("Dejan", String.format("Sending to %s", line));
                                    sendSMS(line, messageText);
                                }
                            } catch (Exception e) {
                                line_failed++;
                                e.printStackTrace();
                            }
                        } else {
                            showSnackbarMesssage(String.format("%s does not exist"), true);
                        }
                    }
                }
            }
        });
    }

    public void sendSMS(String number, String message) {
        try {

            String SENT = "sent";
            String DELIVERED = "delivered";

            Intent sentIntent = new Intent(SENT);
            /*Create Pending Intents*/
            PendingIntent sentPI = PendingIntent.getBroadcast(
                    getApplicationContext(), 0, sentIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            Intent deliveryIntent = new Intent(DELIVERED);

            PendingIntent deliverPI = PendingIntent.getBroadcast(
                    getApplicationContext(), 0, deliveryIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            /* Register for SMS send action */
            registerReceiver(new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {

                    switch (getResultCode()) {

                        case Activity.RESULT_OK:
                            sent++;
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            failed++;
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            failed++;
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            failed++;
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            failed++;
                            break;
                    }
                    Log.d("Dejan", String.format("Sent: %d, Failed: %d", sent, failed));

                    status_received++;
                    if (status_received == length) {
                        showSnackbarMesssage(
                                String.format(
                                        "%d messages sent\n%d failed\n%d invalid numbers\nread from %d files",
                                        sent,
                                        failed,
                                        line_failed,
                                        messagesFolder.list().length
                                ), false);
                        spinner.dismiss();
                    }
                }

            }, new IntentFilter(SENT));
            /* Register for Delivery event */
            registerReceiver(new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.e("SMS status", "Delivered");
                }

            }, new IntentFilter(DELIVERED));

            /*Send SMS*/

            ArrayList<String> parts = manager.divideMessage(message);
            manager.sendMultipartTextMessage(number, null, parts, null, null);
        } catch (Exception ex) {
            if (spinner != null)
                spinner.dismiss();
            ex.printStackTrace();
            showSnackbarMesssage(ex.getMessage(), true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionManager.handlePermissionResult(requestCode, grantResults);
    }

    public void showSnackbarMesssage(String message, boolean error) {
        final Snackbar snackBar = Snackbar.make(findViewById(R.id.activity_main), message, Snackbar.LENGTH_INDEFINITE);
        View snackBarView = snackBar.getView();
        snackBarView.setBackgroundColor(error ? Color.parseColor("#76160c") : ContextCompat.getColor(this, R.color.colorPrimary));
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(5);
        snackBar.setAction("Dismiss", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();
            }
        });
        snackBar.show();
    }

}
